use core::ops::Not;
use core::ops::Add;
use core::ops::Sub;
use core::cmp::PartialOrd;
use core::cmp::Ordering;
use core::fmt::Debug;
use core::num::Wrapping;

use num_traits::Zero;
use num_traits::One;

const SHIFTS : u64 = 63;
const LAST_BIT : u64 = 1 << SHIFTS;

trait Max {
    fn max() -> Self;
}

impl Max for u64 {
    #[inline]
    fn max() -> Self {
        u64::MAX
    }
}

#[derive(Debug, PartialEq)]
enum Sign {
    Positive,
    Negative
}

impl Not for Sign {
    type Output = Sign;

    fn not(self) -> Self::Output {
        match self {
            Sign::Positive => Sign::Negative,
            Sign::Negative => Sign::Positive,
        }
    }
}

#[derive(Debug)]
struct BigIntData(Vec<u64>);

impl BigIntData {
    fn at_pos(&self, pos: usize) -> u64 {
        if pos < self.0.len() { self.0[pos] } else { 0 }
    }

    fn len(&self) -> usize {
        self.0.len()
    }

    fn trim(mut self) -> Self {
        while self.len() > 0 && self.at_pos(self.len()-1) == 0 {
            self.0.pop();
        }

        self
    }
}

impl Zero for BigIntData {
    fn zero() -> BigIntData {
        BigIntData(vec![0])
    }

    fn is_zero(&self) -> bool {
        (self.0.len() == 1 && self.0[0] == 0) || self.0.len() == 0
    }
}

impl core::ops::Add<BigIntData> for BigIntData {
    type Output = BigIntData;

    fn add(self, o: BigIntData) -> Self::Output {
        let mut result = Vec::new();
        let mut carry = 0;
        let mut digit;

        let target = self.0.len().max(o.0.len());

        for i in 0..target {
            (digit, carry) = add_with_carry(self.at_pos(i), o.at_pos(i), carry);

            result.push(digit);
        }

        if carry > 0 {
            result.push(carry);
        }

        BigIntData(result)
    }
}

impl core::ops::Sub<BigIntData> for BigIntData {
    type Output = Option<BigIntData>;

    fn sub(self, o: BigIntData) -> Self::Output {
        if self < o {
            return None;
        }

        let mut result = Vec::new();
        let mut borrow = 0;
        let mut digit;

        let target = self.len();

        for i in 0..target {
            (digit, borrow) = if self.at_pos(i) < borrow {
                ((Wrapping(self.at_pos(i)) - Wrapping(borrow)).0, 1)
            } else {
                sub_with_borrow(self.at_pos(i) - borrow, o.at_pos(i))
            };

            result.push(digit);
        }

        Some(BigIntData(result).trim())
    }
}

impl core::ops::Mul<BigIntData> for BigIntData {
    type Output = BigIntData;

    fn mul(self, o: BigIntData) -> Self::Output {
        if self.is_zero() || o.is_zero() {
            BigIntData(vec![0])
        } else {
            let mut result: Vec<u64> = vec![0; self.len() + o.len()];
            let mut carry = 0;

            for i in 0..self.len() {
                for j in 0..o.len() {
                    let (d1, c1) = mul_with_carry(self.at_pos(i), o.at_pos(j));
                    let (digit, c2) = add_with_carry(d1, carry, 0);

                    carry = c1 + c2;
                    result[i+j] = digit;
                }

                let mut k = i + o.len();
                while carry > 0 {
                    let (digit, c) = add_with_carry(result[k], carry, 0);
                    result[k] = digit;

                    k += 1;
                    carry = c;
                }
            }

            BigIntData(result).trim()
        }
    }
}

impl core::ops::Mul<u64> for BigIntData {
    type Output = BigIntData;

    fn mul(self, o: u64) -> Self::Output {
        self * BigIntData(vec![o])
    }
}

impl core::ops::Add<u64> for BigIntData {
    type Output = BigIntData;

    fn add(self, o: u64) -> Self::Output {
        self + BigIntData(vec![o])
    }
}

#[derive(Debug)]
pub struct BigInt {
    data: BigIntData,
    sign: Sign
}

impl BigInt {
    pub fn new() -> Self {
        Self::from(0)
    }
}

impl From<u64> for BigInt {
    fn from(i: u64) -> Self {
        BigInt {
            data: BigIntData(vec![i]),
            sign: Sign::Positive
        }
    }
}

impl From<Vec<u64>> for BigInt {
    fn from(i: Vec<u64>) -> Self {
        BigInt {
            data: BigIntData(i),
            sign: Sign::Positive
        }
    }
}


impl From<&str> for BigInt {
    fn from(s: &str) -> Self {
        let mut data = BigIntData(Vec::new());
        let mut skip = 0;

        let sign = if s.chars().nth(0) == Some('-') {
            skip = 1;
            Sign::Negative
        } else {
            Sign::Positive
        };

        for c in s.chars().skip(skip) {
            data = data * 10 + (c.to_digit(10).unwrap() as u64);
        }

        BigInt {
            data: data,
            sign: sign
        }
    }
}

impl ToString for BigInt {
    fn to_string(&self) -> String {
        let mut data = self.data.0.clone();
        let mut digits : Vec<u8> = vec![0];

        while data.len() > 0 {
            let z = data.len() - 1;

            for _ in 0..SHIFTS+1 {
                let mut carry: u8 = ((data[z] & LAST_BIT) >> SHIFTS) as u8;
                data[z] <<= 1;

                for i in 0..digits.len() {
                    digits[i] <<= 1;
                    digits[i] += carry;

                    if digits[i] >= 10 {
                        carry = 1;
                        digits[i] -= 10;
                    } else {
                        carry = 0;
                    }
                }

                if carry > 0 {
                    digits.push(1);
                }
           }

            data.pop();
        }

        let mut chars : Vec<u8> = digits.iter().rev().map(|i| -> u8 { *i + ('0' as u8) }).collect();

        if self.sign == Sign::Negative {
            chars.insert(0, '-' as u8);
        }

        String::from_utf8(chars).unwrap()
    }
}

impl PartialEq<BigIntData> for BigIntData {
    fn eq(&self, o: &BigIntData) -> bool {
        for i in 0..self.len().max(o.len()) {
            if self.at_pos(i) != o.at_pos(i) {
                return false;
            }
        }

        true
    }
}

impl PartialOrd<BigIntData> for BigIntData {
    fn partial_cmp(&self, other: &BigIntData) -> Option<Ordering> {
        let target = usize::max(self.len(), other.len());
        for i in (0..target).rev() {
            if self.at_pos(i) > other.at_pos(i) {
                return Some(Ordering::Greater);
            } else if self.at_pos(i) < other.at_pos(i) {
                return Some(Ordering::Less);
            }
        }

        Some(Ordering::Equal)
    }
}

impl PartialEq<BigInt> for BigInt {
    fn eq(&self, o: &BigInt) -> bool {
        if self.data.is_zero() && o.data.is_zero() {
            return true;
        }

        if self.sign != o.sign {
            return false;
        }

        self.data == o.data
    }
}

impl PartialOrd<BigInt> for BigInt {
    fn partial_cmp(&self, other: &BigInt) -> Option<Ordering> {
        if self.data.is_zero() && other.data.is_zero() {
            Some(Ordering::Equal)
        } else if self.sign == Sign::Positive {
            if other.sign == Sign::Negative {
                Some(Ordering::Greater)
            } else {
                self.data.partial_cmp(&other.data)
            }
        } else {
            if other.sign == Sign::Positive {
                Some(Ordering::Less)
            } else {
                match self.data.partial_cmp(&other.data) {
                    Some(Ordering::Less) => Some(Ordering::Greater),
                    Some(Ordering::Greater) => Some(Ordering::Less),
                    Some(Ordering::Equal) => Some(Ordering::Equal),
                    None => None
                }
            }
        }
    }
}

impl core::ops::Add<BigInt> for BigInt {
    type Output = BigInt;

    fn add(self, o: BigInt) -> Self::Output {
        if self.sign == o.sign {
            BigInt {
                data: self.data + o.data,
                sign: self.sign
            }
        } else if self.data > o.data {
            BigInt {
                data: (self.data - o.data).unwrap(),
                sign: self.sign
            }
        } else {
            BigInt {
                data: (o.data - self.data).unwrap(),
                sign: o.sign
            }
        }
    }
}

impl core::ops::Sub<BigInt> for BigInt {
    type Output = BigInt;

    fn sub(self, o: BigInt) -> Self::Output {
        if self.sign != o.sign {
            BigInt {
                data: self.data + o.data,
                sign: self.sign
            }
        } else if self.data > o.data {
            BigInt {
                data: (self.data - o.data).unwrap(),
                sign: self.sign
            }
        } else {
            BigInt {
                data: (o.data - self.data).unwrap(),
                sign: !o.sign
            }
        }
    }
}

impl core::ops::Mul<BigInt> for BigInt {
    type Output = BigInt;

    fn mul(self, o: BigInt) -> Self::Output {
        if self.sign == o.sign {
            BigInt {
                data: self.data * o.data,
                sign: Sign::Positive
            }
        } else {
            BigInt {
                data: self.data * o.data,
                sign: Sign::Negative
            }
        }
    }
}

fn add_with_carry<T>(a: T, b: T, carry: T) -> (T, T)
where T: PartialOrd + Add<Output=T> + Not<Output=T> + Sub<Output=T> + Zero + One + Copy + Debug,
      Wrapping<T>: Add<Output=Wrapping<T>>
{
    fn add_cond<T>(x: T, y: T, carry: T) -> (T, T)
    where T: PartialOrd + Add<Output=T> + Not<Output=T> + Sub<Output=T> + Zero + One + Copy + Debug,
          Wrapping<T>: Add<Output=Wrapping<T>>
    {
        if y + carry <= !x {
            (x + y + carry, T::zero())
        } else {
            ((Wrapping(y) + Wrapping(x) + Wrapping(carry)).0, T::one())
        }
    }

    if carry <= !b {
        add_cond::<T>(a, b, carry)
    } else if carry <= !a {
        add_cond::<T>(b, a, carry)
    } else {
        (a, T::one())
    }
}


fn sub_with_borrow<T>(a: T, b: T) -> (T, T)
where T: PartialOrd + Add<Output=T> + Not<Output=T> + Sub<Output=T> + Zero + One + Copy + Debug + Max,
      Wrapping<T>: Sub<Output=Wrapping<T>>
{
    if b <= a {
        (a - b, T::zero())
    } else {
        ((Wrapping(a) - Wrapping(b)).0, T::one())
    }
}

fn mul_with_carry(a: u64, b: u64) -> (u64, u64) {
    fn hi(x: u64) -> u64 { (x & 0xFFFFFFFF00000000) >> 32 }
    fn lo(x: u64) -> u64 { x & 0x00000000FFFFFFFF }
    fn to_hi(x: u64) -> u64 { lo(x) << 32 }

    fn simple_mul(x: u64, y: u64) -> (u64, u64) {
        let res = x * y;
        (lo(res), hi(res))
    }

    let (ah, al) = (hi(a), lo(a));
    let (bh, bl) = (hi(b), lo(b));

    let p : [(u64, u64); 4] = [simple_mul(al, bl), simple_mul(ah, bl), simple_mul(al, bh), simple_mul(ah, bh)];

    let result_lo = p[0].0;
    let result_hi = p[0].1 + p[1].0 + p[2].0;
    let carry_lo = p[1].1 + p[2].1 + p[3].0 + hi(result_hi);
    let carry_hi = p[3].1 + hi(carry_lo);

    (to_hi(result_hi) + lo(result_lo), to_hi(carry_hi) + lo(carry_lo))
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_basic_eq() {
        assert_eq!(BigInt::new(), BigInt::new());
        assert_eq!(BigInt::new(), BigInt::from(0));
        assert_eq!(BigInt::from(0), BigInt::from(0));
        assert_eq!(BigInt::from(1), BigInt::from(1));
        assert_ne!(BigInt::from(0), BigInt::from(1));
    }

    #[test]
    fn test_basic_to_string() {
        assert_eq!(BigInt::from(0).to_string(), "0");
        assert_eq!(BigInt::from(1).to_string(), "1");
        assert_eq!(BigInt::from(10).to_string(), "10");
        assert_eq!(BigInt::from(255).to_string(), "255");
    }

    #[test]
    fn test_basic_add() {
        assert_eq!(BigInt::from(0) + BigInt::from(0), BigInt::from(0));
        assert_eq!(BigInt::from(0) + BigInt::from(1), BigInt::from(1));
    }

    #[test]
    fn test_basic_sub() {
        assert_eq!(BigInt::from(0) - BigInt::from(0), BigInt::from(0));
        assert_eq!(BigInt::from(1) - BigInt::from(0), BigInt::from(1));
        assert_eq!(BigInt::from(1) - BigInt::from(1), BigInt::from(0));
        assert_eq!(BigInt::from(0) - BigInt::from(1), BigInt { data: BigIntData(vec![1]), sign: Sign::Negative });
    }

    #[test]
    fn test_bigger_sub() {
        assert_eq!(BigInt::from(vec![0, 1]) - BigInt::from(vec![0]), BigInt::from(vec![0, 1]));
        assert_eq!(BigInt::from(vec![0, 1]) - BigInt::from(vec![1]), BigInt::from(vec![u64::MAX]));
        assert_eq!(BigInt::from(vec![0, 1]) - BigInt::from(vec![1]), BigInt::from(vec![u64::MAX]));
        assert_eq!(BigInt::from(vec![0, 0, 0, 1]) - BigInt::from(vec![1]), BigInt::from(vec![u64::MAX, u64::MAX, u64::MAX]));
    }

    #[test]
    fn test_zero_cmp() {
        assert_eq!(BigInt { data: BigIntData(vec![0]), sign: Sign::Positive }, BigInt { data: BigIntData(vec![0]), sign: Sign::Positive });
        assert_eq!(BigInt { data: BigIntData(vec![0]), sign: Sign::Negative }, BigInt { data: BigIntData(vec![0]), sign: Sign::Negative });
        assert_eq!(BigInt { data: BigIntData(vec![0]), sign: Sign::Positive }, BigInt { data: BigIntData(vec![0]), sign: Sign::Negative });
        assert_eq!(BigInt { data: BigIntData(vec![0]), sign: Sign::Negative }, BigInt { data: BigIntData(vec![0]), sign: Sign::Positive });
    }

    #[test]
    fn test_basic_cmp() {
        assert!(BigInt::from(vec![0, 1]) > BigInt::from(vec![0]));
        assert!(BigInt { data: BigIntData(vec![1]), sign: Sign::Positive } > BigInt { data: BigIntData(vec![1]), sign: Sign::Negative });
        assert!(BigInt { data: BigIntData(vec![1]), sign: Sign::Negative } < BigInt { data: BigIntData(vec![1]), sign: Sign::Positive });
        assert!(BigInt { data: BigIntData(vec![1]), sign: Sign::Negative } == BigInt { data: BigIntData(vec![1]), sign: Sign::Negative });
        assert!(BigInt { data: BigIntData(vec![1, 1]), sign: Sign::Negative } < BigInt { data: BigIntData(vec![1]), sign: Sign::Negative });
    }

    #[test]
    fn test_bigger_eq() {
        assert_eq!(BigInt::from(vec![1, 0]), BigInt::from(vec![1, 0]));
        assert_eq!(BigInt::from(vec![1, 1]), BigInt::from(vec![1, 1]));
        assert_ne!(BigInt::from(vec![0, 1]), BigInt::from(vec![1, 1]));
        assert_ne!(BigInt::from(vec![1, 0]), BigInt::from(vec![1, 1]));
        assert_ne!(BigInt::from(vec![1, 0]), BigInt::from(vec![0, 1]));
        assert_ne!(BigInt::from(vec![1, 0]), BigInt::from(vec![0, 0]));
    }

    #[test]
    fn test_bigger_to_string() {
        assert_eq!(BigInt::from(vec![0, 1]).to_string(), "18446744073709551616");
        assert_eq!(BigInt::from(vec![1, 1]).to_string(), "18446744073709551617");
        assert_eq!(BigInt::from(vec![0, 2]).to_string(), "36893488147419103232");
        assert_eq!(BigInt::from(vec![0, 0, 1]).to_string(), "340282366920938463463374607431768211456");
    }

    #[test]
    fn test_bigger_arith() {
        assert_eq!(BigInt::from(vec![0, 1]) + BigInt::from(vec![1]), BigInt::from(vec![1, 1]));
        assert_eq!(BigInt::from(vec![128]) + BigInt::from(vec![128-1]), BigInt::from(vec![255]));
    }

    #[test]
    fn test_add_with_carry_0() {
        let result = add_with_carry::<u64>(0, 0, 0);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_add_with_carry_1() {
        let result = add_with_carry::<u64>(0, 0, 1);

        assert_eq!(result.0, 1);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_add_with_carry_2() {
        let result = add_with_carry::<u64>(1, 0, 1);

        assert_eq!(result.0, 2);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_add_with_carry_3() {
        let result = add_with_carry::<u64>(0, 1, 1);

        assert_eq!(result.0, 2);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_add_with_carry_4() {
        let result = add_with_carry::<u64>(u64::MAX-1, 0, 0);

        assert_eq!(result.0, u64::MAX-1);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_add_with_carry_5() {
        let result = add_with_carry::<u64>(u64::MAX-1, 1, 0);

        assert_eq!(result.0, u64::MAX);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_add_with_carry_6() {
        let result = add_with_carry::<u64>(u64::MAX-1, 1, 0);

        assert_eq!(result.0, u64::MAX);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_add_with_carry_7() {
        let result = add_with_carry::<u64>(u64::MAX, 1, 0);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_add_with_carry_8() {
        let result = add_with_carry::<u64>(u64::MAX, 2, 0);

        assert_eq!(result.0, 1);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_add_with_carry_9() {
        let result = add_with_carry::<u64>(u64::MAX, 0, 1);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_add_with_carry_10() {
        let result = add_with_carry::<u64>(1, u64::MAX, 0);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_add_with_carry_11() {
        let result = add_with_carry::<u64>(0, u64::MAX, 1);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_add_with_carry_12() {
        let result = add_with_carry::<u64>(u64::MAX, u64::MAX, 1);

        assert_eq!(result.0, u64::MAX);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_sub_with_borrow_1() {
        let result = sub_with_borrow::<u64>(0, 0);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_sub_with_borrow_2() {
        let result = sub_with_borrow::<u64>(1, 0);

        assert_eq!(result.0, 1);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_sub_with_borrow_3() {
        let result = sub_with_borrow::<u64>(1, 1);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_sub_with_borrow_4() {
        let result = sub_with_borrow::<u64>(0, 1);

        assert_eq!(result.0, u64::MAX);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_sub_with_borrow_5() {
        let result = sub_with_borrow::<u64>(u64::MAX-1, u64::MAX);

        assert_eq!(result.0, u64::MAX);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_mul_with_carry_1() {
        let result = mul_with_carry(1, 1);

        assert_eq!(result.0, 1);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_mul_with_carry_2() {
        let result = mul_with_carry(0, 1);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_mul_with_carry_3() {
        let result = mul_with_carry(1, 0);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_mul_with_carry_4() {
        let result = mul_with_carry(0x8000000000000000, 2);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 1);
    }

    #[test]
    fn test_mul_with_carry_5() {
        let result = mul_with_carry(0x8000000000000000, 4);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 2);
    }

    #[test]
    fn test_mul_with_carry_6() {
        let result = mul_with_carry(0x8000000000000000, 0x8000000000000000);

        assert_eq!(result.0, 0);
        assert_eq!(result.1, 0x4000000000000000);
    }

    #[test]
    fn test_mul_with_carry_7() {
        let result = mul_with_carry(0xFFFFFFFFFFFFFFFF, 0xFFFFFFFFFFFFFFFF);

        assert_eq!(result.0, 0x0000000000000001);
        assert_eq!(result.1, 0xFFFFFFFFFFFFFFFE);
    }

    #[test]
    fn test_mul_with_carry_8() {
        let result = mul_with_carry(0x000000000000000F, 0x000000000000000F);

        assert_eq!(result.0, 0x00000000000000E1);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_mul_with_carry_9() {
        let result = mul_with_carry(0x0F00000000000000, 0x000000000000000F);

        assert_eq!(result.0, 0xE100000000000000);
        assert_eq!(result.1, 0);
    }

    #[test]
    fn test_mul_bigintdata_1() {
        let result = BigIntData(vec![1]) * BigIntData(vec![1]);

        assert_eq!(result, BigIntData(vec![1]));
    }

    #[test]
    fn test_mul_bigintdata_2() {
        let result = BigIntData(vec![0]) * BigIntData(vec![1]);

        assert_eq!(result, BigIntData(vec![0]));
    }

    #[test]
    fn test_mul_bigintdata_3() {
        let result = BigIntData(vec![1]) * BigIntData(vec![0]);

        assert_eq!(result, BigIntData(vec![0]));
    }

    #[test]
    fn test_mul_bigintdata_4() {
        let result = BigIntData(vec![2]) * BigIntData(vec![2]);

        assert_eq!(result, BigIntData(vec![4]));
    }

    #[test]
    fn test_mul_bigintdata_5() {
        let result = BigIntData(vec![0, 1]) * BigIntData(vec![2]);

        assert_eq!(result, BigIntData(vec![0, 2]));
    }

    #[test]
    fn test_mul_bigintdata_6() {
        let result = BigIntData(vec![0, 1]) * BigIntData(vec![0, 1]);

        assert_eq!(result, BigIntData(vec![0, 0, 1]));
    }

    #[test]
    fn test_mul_bigintdata_7() {
        let result = BigIntData(vec![0, 1]) * BigIntData(vec![0, 0, 1]);

        assert_eq!(result, BigIntData(vec![0, 0, 0, 1]));
    }

    #[test]
    fn test_mul_bigintdata_8() {
        let result = BigIntData(vec![0x8000000000000000]) * BigIntData(vec![2]);

        assert_eq!(result, BigIntData(vec![0, 1]));
    }

    #[test]
    fn test_mul_bigintdata_9() {
        let result = BigIntData(vec![0x8000000000000000]) * BigIntData(vec![0, 2]);

        assert_eq!(result, BigIntData(vec![0, 0, 1]));
    }

    #[test]
    fn test_mul_bigint_1() {
        let result = BigInt { data: BigIntData(vec![1]), sign: Sign::Positive } * BigInt { data: BigIntData(vec![1]), sign: Sign::Positive };

        assert_eq!(result, BigInt { data: BigIntData(vec![1]), sign: Sign::Positive });
    }

    #[test]
    fn test_bigint_from_str_1() {
        assert_eq!(BigInt::from("1"), BigInt { data: BigIntData(vec![1]), sign: Sign::Positive });
    }

    #[test]
    fn test_bigint_from_str_2() {
        assert_eq!(BigInt::from("0"), BigInt { data: BigIntData(vec![0]), sign: Sign::Positive });
    }

    #[test]
    fn test_bigint_from_str_3() {
        assert_eq!(BigInt::from("18446744073709551616"), BigInt { data: BigIntData(vec![0, 1]), sign: Sign::Positive });
    }

    #[test]
    fn test_bigint_from_str_4() {
        assert_eq!(BigInt::from("-1"), BigInt { data: BigIntData(vec![1]), sign: Sign::Negative });
    }

    #[test]
    fn test_bigint_from_str_5() {
        assert_eq!(BigInt::from("-18446744073709551616"), BigInt { data: BigIntData(vec![0, 1]), sign: Sign::Negative });
    }

    #[test]
    fn test_bigint_reflexive_to_str_1() {
        assert_eq!(BigInt::from("18446744073709551616").to_string(), "18446744073709551616");
    }

    #[test]
    fn test_bigint_reflexive_to_str_2() {
        assert_eq!(BigInt::from("-1").to_string(), "-1");
    }
}
